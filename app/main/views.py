
from flask import render_template, flash, request, session, redirect, url_for
from . import main
from .. import db
from datetime import datetime
from flask_paginate import Pagination,get_page_parameter,get_page_args
from ..models import Contact, City
from .forms import ContactForm



@main.context_processor
def inject_now():
	return {'now':datetime.utcnow()}

@main.route('/',methods=['GET'])
def index():
	return redirect(url_for('main.contacts',page=1))

    	
@main.route('/contacts/')
def contacts(page=1):
	joincondition=(Contact.cityid==City.id)
	
	contacts=db.session.query(Contact).join(City,(joincondition)).order_by(Contact.id.desc())
	
	total=contacts.count()
	
	per_page=2
	
	page=int(request.args.get("page",1))
	
	factor=page-1
	
	ofset=factor*per_page
	
	shown_contacts=contacts.limit(per_page).offset(ofset)
	
	paging=Pagination(page=page,per_page=per_page,offset=ofset,total=total,record_name=contacts,css_framework='foundation')
	
	return render_template('contacts.html',contacts=shown_contacts,page=page,offset=ofset,pagination=paging)
	
	    	
@main.route('/contacts/edit/<int:id>',methods=['GET','POST'])
def editContact(id):
    form=ContactForm(csrf_enabled=False)
       
    my_contact = db.session.query(Contact).get(id)
    
    city_id=my_contact.cityid
    cities=[(c.id,c.cityname) for c in db.session.query(City).all()]
    mycity_name = db.session.query(City).get(city_id)
    my_city=(city_id,mycity_name) 
    
    if request.method == 'GET' and my_contact is not None:
    	  cities=[(c.id,c.cityname) for c in db.session.query(City).all()]
    	  form.name.data = my_contact.name
    	  form.firstname.data = my_contact.firstname
    	  form.email.data = my_contact.email
    	  form.street.data = my_contact.street
    	  form.zip_code.data = my_contact.zip_code
    	  form.city.choices=cities
    	  form.city.default=my_city
    	  form.city.data = my_city[0]
    	     	  
    elif request.method=='GET' and my_contact is None:
    	print("no saved contact")
    	form.city.choices=cities
    
    	   
    	
    form.city.choices=cities
    
    if form.validate_on_submit():
           
        name=form.name.data        
        firstname=form.firstname.data
        email=form.email.data
        street=form.street.data
        zip_code=form.zip_code.data
        city=form.city.data
        
        db.session.query(Contact).filter_by(id=id).update({'name':name,
        'firstname':firstname,
        'street':street,
        'zip_code':zip_code,
        'cityid':city}
        )
        
        db.session.commit()
    
        flash('The contact details %s have been updated' %name,'success')  
        
    return render_template('contact_edit.html',form=form)

    
            	    	
@main.route('/contacts/create',methods=['GET','POST'])
def createContact():
	form=ContactForm(csrf_enabled=False)
	cities=[(c.id,c.cityname) for c in db.session.query(City).all()]
	quest=(0,"Select City")
	form.city.choices=cities
	form.city.default=quest
	
	if form.validate_on_submit():
		name=form.name.data
		firstname=form.firstname.data
		email=form.email.data
		street=form.street.data
		zip_code=form.zip_code.data
		city=form.city.data
		contact=Contact(name=name,firstname=firstname,email=email,street=street,zip_code=zip_code,cityid=city)
		db.session.add(contact)
		db.session.commit()
		
		flash('The new contact %s has been saved' %name,'success')
		
	return render_template('contact_create.html',form=form)
