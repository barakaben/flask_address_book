
#from flask_sqlalchemy import SQLAlchemy
from app import db
#db=SQLAlchemy()

class Contact(db.Model):
    __tablename__='contacts'
    
    id=db.Column(db.Integer(),primary_key=True,autoincrement=True)
    name=db.Column(db.String(55),index=True)
    firstname=db.Column(db.String(20))
    email=db.Column(db.String())
    street=db.Column(db.String(25))
    zip_code=db.Column(db.Integer())
    cityid=db.Column(db.Integer,db.ForeignKey('cities.id'))
    city=db.relationship('City', backref='contacts')
    
    def __repr__(self):
        return '<contact % r>' %self.name

class City(db.Model):
     __tablename__='cities'
     id=db.Column(db.Integer(),primary_key=True,autoincrement=True)
     cityname=db.Column(db.String(40),unique=True)
     
     def __repr__(self):
         return '<city % r>' %self.cityname #.name        	    

