
from app import db
from sampledata import cities, contacts
from app.models import City, Contact

class Seeder():
	
	def __init__(self,cities=cities,contacts=contacts):
		self.cities=cities
		self.contacts=contacts
		
	def _getCities(self):
		return self.cities
		
	def _getContacts(self):
		return self.contacts
	
	def city_seed(self,db=db,City=City):
		cities=self._getCities()

   	for cit in cities:
   		print("adding %s into cities table"%cit)
   		cityname=cit
   		city=City(cityname=cityname)
   		db.session.add(city)		
	
	def contact_seed(self,db=db,Contact=Contact):
		contacts=self._getContacts()
   	for contacti in contacts:
   		print("adding %s into contacts table"%contacti)
   		name=contacti[0]
   		firstname=contacti[1]
   		email=contacti[2]
   		street=contacti[3]
   		zip_code=contacti[4]
   		cityid=contacti[5]
   		contact=Contact(name=name,firstname=firstname,email=email,street=street,zip_code=zip_code,cityid=cityid)
   		db.session.add(contact)		    
